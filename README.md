# SNT_seconde

cours de Mme Ghesquière : cecile.ghesquiere@ac-lille.fr  

lien vers framapad des questions : [ici](https://mensuel.framapad.org/p/zdikrjhiaz-a5wy?lang=fr)

lien vers le réseau du lycée : [ici](https://194.167.100.29:8443/owncloud/index.php)  


![](./img/donnes.jpg)
# Données structurées
* [groupes sanguins](./DONNEES STRUCTUREES/groupes_sanguins.md)
* [Parcours algorea](https://parcours.algorea.org) :
    * rejoindre SNT nouvelle version : ![](./img/snt_nv.jpg)
    * créer son profil en se créant un compte ![](./img/se_connecter.jpg)
    * choisir l'onglet 'mes groupes' : ![](./img/grpe.jpg)
    * voici le code pour rejoindre le groupe : 7uga9qd4vt
---------------------------
![](./img/photo.jpg)
# Photographie
* [Appareil photo](./PHOTOGRAPHIE/0_appareil_photo_eleve.pdf)  
lien [vidéo](https://www.youtube.com/watch?v=pEtUsh7DI9k&feature=youtu.be)    
* [Images](./PHOTOGRAPHIE/1_IMAGES_eleve.pdf)
* TP [dématriçage](/PHOTOGRAPHIE/2_TP_dematricage.pdf) , fichier [libreOffice](./PHOTOGRAPHIE/2_Dematricage_ELEVE.xls) et son [corrigé](./PHOTOGRAPHIE/2_Dematricage_corrige.xls)  

---------------------------
![](./img/web.jpg)
# Web
## Cours  
* [cours à compléter](./WEB/cours/cours_web-version_élève.pdf)  
* [Activité Page Rank](./WEB/cours/activité%20Page%20rank%20elève.pdf) et le [fichier Python à compléter](./WEB/cours/pagerank_activite_el.py)  
* [liste de questions à travailler](https://mensuel.framapad.org/p/opffztw1yx-a3kk?lang=fr)
## TP1 : découverte du langage html  
* [sujet de TP](./WEB/TP_web_1/TPweb1-eleve.pdf)  
* [fichier html](./WEB/TP_web_1/TPweb1.html)

## TP2 : réalisation d'une page WEB  
* [sujet de TP](./WEB/TP_web_2/TPweb2-eleve.pdf)  
* [fichier html à télécharger](./WEB/TP_web_2/texte brut meringues.html) et [l'image à télécharger](./WEB/TP_web_2/meringues.jpg)  

## TP3 : découverte du langage css  
* [sujet de TP](./WEB/TP_web_3/TPweb3.pdf)
* [fichier html à télécharger](./WEB/TP_web_3/TPweb3.html) ainsi que le [fichier .css](./WEB/TP_web_3/style.css) et l'[image](https://framagit.org/CecGhesq/snt_seconde/-/blob/master/WEB/TP_web_3/logo.png)


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Les  documents sont mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.




